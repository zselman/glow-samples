#include "shader.hh"

layout(local_size_x = CLEAR_SHADER_SIZE, local_size_y = CLEAR_SHADER_SIZE, local_size_z = CLEAR_SHADER_SIZE) in;

#include "shader-header.glsl"

void main()
{
    ivec3 id = ivec3(gl_GlobalInvocationID);
    
    imageStore(uTexDistanceField, id, uvec4(UDISTANCE_MAX));
    imageStore(uTexNearestPoint, id, uvec4(UDISTANCE_MAX));
}
