#include "noise2D.glsl"

uniform float uAmplitude;
uniform float uWavelength;
uniform float uAmplitudeFactor;
uniform float uWavelengthFactor;
uniform int uMaxLevels;


uint wang_hash(uint seed)
{
    seed = (seed ^ 61u) ^ (seed >> 16u);
    seed *= 9u;
    seed = seed ^ (seed >> 4u);
    seed *= 0x27D4EB2Du;
    seed = seed ^ (seed >> 15u);
    return seed;
}

float wang_float(uint hash)
{
    return hash / float(0x7FFFFFFF) / 2.0;
}


float terrainAt(float x, float y, float edgeLength)
{
    float A = uAmplitude;
    float E = uWavelength;
    float amplF = uAmplitudeFactor;
    float freqF = uWavelengthFactor;

    float offsX = 0.0;
    float offsY = 0.0;
    uint seed = 0x42;

    float h = 0.0;

    for (int i = 0; i < uMaxLevels; ++i)
    {
        h += cnoise(vec2(x, y) / E + vec2(offsX, offsY)) * A;

        E *= freqF;
        if (E < edgeLength)
            break;
            
        A *= amplF;
        offsX = wang_float(seed);
        seed = wang_hash(seed);
        offsY = wang_float(seed);
        seed = wang_hash(seed);
    }

    return h;
}
