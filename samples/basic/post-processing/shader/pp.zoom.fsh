uniform sampler2DRect uTexture;

uniform float uZoomLvl;
uniform vec2 uMousePos;

out vec4 fColor;

void main() 
{
    ivec2 mp = ivec2(uMousePos);

    vec4 color = texelFetch(uTexture, (ivec2(gl_FragCoord.xy) - mp) / int(uZoomLvl) + mp);
    fColor = color;
}
