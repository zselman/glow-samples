uniform sampler2DRect uTexture;

uniform float uVignetteA;
uniform float uVignetteB;

in vec2 vPosition;

out vec4 fColor;

void main() 
{
    vec4 color = texelFetch(uTexture, ivec2(gl_FragCoord.xy));

    float d = distance(vPosition, vec2(0.5));
    color *= smoothstep(uVignetteA, uVignetteB, d);

    fColor = color;
}
