uniform sampler2D uNormalsBase;
uniform sampler2D uNormalsDetail;

uniform vec3 uLightDir;
uniform vec3 uCamPos;

uniform int uPos;
uniform int uMode;
uniform bool uShowNormals;
uniform bool uSwitchMaps;

vec3 uAmbientColor = vec3(.1);
vec3 uLightColor = vec3(1);

in vec3 vPosition;
in vec3 vNormal;
in vec3 vTangent;
in vec2 vTexCoord;

out vec3 fColor;

vec3 unpackNormalmap(vec4 texel)
{
    texel.xy = texel.xy * 2 - 1 - 1 / 255.;
    return texel.xyz;
}

void main() {
    vec3 L = normalize(uLightDir);
    vec3 V = normalize(uCamPos - vPosition);
    vec3 N = normalize(vNormal);
    vec3 T = normalize(vTangent);
    vec3 B = normalize(cross(vTangent, vNormal));
    mat3 tbn = mat3(T, B, N);

    vec3 N1, N2;

    vec4 nmBaseT = texture(uNormalsBase, vTexCoord);
    vec4 nmDetailT = texture(uNormalsDetail, vTexCoord * 3);
    vec3 nmBase = unpackNormalmap(nmBaseT);
    vec3 nmDetail = unpackNormalmap(nmDetailT);

    if (uSwitchMaps)
    {
        vec3 tmp = nmBase;
        nmBase = nmDetail;
        nmDetail = tmp;
    }
    if (uSwitchMaps)
    {
        vec4 tmp = nmBaseT;
        nmBaseT = nmDetailT;
        nmDetailT = tmp;
    }

    switch(uPos) {
    case 0:
        N = normalize(tbn * nmBase);
        break;
    case 1:
        switch(uMode) {
        case 0: // base
            N = normalize(tbn * nmBase);
            break;
        case 1: // detail
            N = normalize(tbn * nmDetail);
            break;
        case 2: // linear
            N = normalize(tbn * (nmBase + nmDetail));
            break;
        case 3: // partial derivatives
            vec2 pd = nmBase.xy / nmBase.z + nmDetail.xy / nmDetail.z;
            N = normalize(tbn * vec3(pd, 1));
            break;
        case 4: // whiteout
            N = normalize(tbn * vec3(nmBase.xy + nmDetail.xy, nmBase.z * nmDetail.z));
            break;
        case 5: // RNM
            vec3 rt = nmBaseT.xyz * vec3(2, 2, 2) + vec3(-1, -1, 0);
            vec3 ru = nmDetailT.xyz * vec3(-2, -2, 2) + vec3(1, 1, -1);
            vec3 rr = rt * dot(rt, ru) - ru * rt.z;
            N = normalize(tbn * rr);
            break;
        }
        break;
    case 2:
        N = normalize(tbn * nmDetail);
        break;
    }

    vec3 R = reflect(-L, N);
    vec3 baseColor = vec3(199 / 255., 221 / 255., 242 / 255.);

    vec3 color = baseColor * uAmbientColor;

    color += baseColor * uLightColor * max(0.0, dot(L, N));

    color += baseColor * uLightColor * pow(max(0.0, dot(R, V)), 32);

    fColor = pow(color, vec3(1 / 2.224));

    if (uShowNormals)
        fColor = N;
}
