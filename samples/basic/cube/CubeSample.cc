#include "CubeSample.hh"

#include <imgui/imgui.h>

#include <typed-geometry/tg.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/VertexArray.hh>

#include <glow-extras/geometry/Cube.hh>

using namespace glow;

void CubeSample::init()
{
    GlfwApp::init(); // call to base!

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");
    mCube = geometry::Cube<>().generate();

    getCamera()->setLookAt({2, 5, 4}, {0, 0, 0});
}

void CubeSample::render(float elapsedSeconds)
{
    if (mAnimate)
        mRuntime += elapsedSeconds;

    GLOW_SCOPED(clearColor, 0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GLOW_SCOPED(enable, GL_DEPTH_TEST);

    auto shader = mShader->use();
    shader["uRuntime"] = mRuntime;
    shader["uView"] = getCamera()->getViewMatrix();
    shader["uProj"] = getCamera()->getProjectionMatrix();
    shader["uModel"] = tg::rotation_around(tg::dir3{0, 1, 0}, tg::radians(mRuntime));

    mCube->bind().draw();
}

void CubeSample::onGui()
{
    ImGui::Begin("Cube Sample");
    ImGui::Checkbox("Animate", &mAnimate);
    ImGui::End();
}
