#include "ColoredTriangleSample.hh"

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/geometry/Cube.hh>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

void ColoredTriangleSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!

    struct Vertex
    {
        glm::vec3 position;
        glm::vec3 color;
    };

    std::vector<Vertex> vertices;
    vertices.push_back({glm::vec3(-0.8f, -0.8f, 0.0f), glm::vec3(1, 0, 0)});
    vertices.push_back({glm::vec3(+0.8f, -0.8f, 0.0f), glm::vec3(0, 1, 0)});
    vertices.push_back({glm::vec3(+0.0f, +0.8f, 0.0f), glm::vec3(1, 1, 1)});

    auto ab = ArrayBuffer::create({{&Vertex::position, "aPosition"}, //
                                   {&Vertex::color, "aColor"}});
    ab->bind().setData(vertices);
    mTriangle = VertexArray::create(ab);

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");

    TwAddVarRW(tweakbar(), "Animate", TW_TYPE_BOOLCPP, &mAnimate, "");
}

void ColoredTriangleSample::render(float elapsedSeconds)
{
    GLOW_SCOPED(disable, GL_CULL_FACE);
    GLOW_SCOPED(disable, GL_DEPTH_TEST);

    mRuntime += elapsedSeconds;
    auto time = mAnimate ? mRuntime : 0.0f;

    GLOW_SCOPED(clearColor, 0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    auto shader = mShader->use();
    shader.setUniform("uRuntime", time);

    mTriangle->bind().draw();
}
