uniform vec3 uCamPos;

uniform sampler2D uTexAlbedo;
uniform sampler2D uTexAO;
uniform sampler2D uTexHeight;
uniform sampler2D uTexMetallic;
uniform sampler2D uTexNormal;
uniform sampler2D uTexRoughness;

uniform int uShowTexture;

uniform bool uUseAlbedo;
uniform bool uUseAO;
uniform bool uUseHeight;
uniform bool uUseNormal;
uniform bool uUseMetallic;
uniform bool uUseRoughness;
uniform bool uUseReflection;

uniform samplerCube uCubeMap;

uniform vec3 uLightPos;
uniform float uLightRadius;

in vec2 tTexCoord;
in vec3 tNormal;
in vec3 tTangent;
in vec3 tWorldPos;

out vec3 fColor;

// DO NOT MULTIPLY BY COS THETA
vec3 shadingSpecularGGX(vec3 N, vec3 V, vec3 L, float roughness, vec3 F0)
{
    // see http://www.filmicworlds.com/2014/04/21/optimizing-ggx-shaders-with-dotlh/
    vec3 H = normalize(V + L);

    float dotLH = max(dot(L, H), 0.0);
    float dotNH = max(dot(N, H), 0.0);
    float dotNL = max(dot(N, L), 0.0);
    float dotNV = max(dot(N, V), 0.0);

    float alpha = roughness * roughness;

    // D (GGX normal distribution)
    float alphaSqr = alpha * alpha;
    float denom = dotNH * dotNH * (alphaSqr - 1.0) + 1.0;
    float D = alphaSqr / (denom * denom);
    // no pi because BRDF -> lighting

    // F (Fresnel term)
    float F_a = 1.0;
    float F_b = pow(1.0 - dotLH, 5); // manually?
    vec3 F = mix(vec3(F_b), vec3(F_a), F0);

    // G (remapped hotness, see Unreal Shading)
    float k = (alpha + 2 * roughness + 1) / 8.0;
    float G = dotNL / (mix(dotNL, 1, k) * mix(dotNV, 1, k));
    // '* dotNV' - canceled by normalization

    // '/ dotLN' - canceled by lambert
    // '/ dotNV' - canceled by G
    return D * F * G / 4.0;
}

void main() 
{
    // calculate dirs
    vec3 N = normalize(tNormal);
    vec3 V = normalize(uCamPos - tWorldPos);
    vec3 R = reflect(-V, N);
    vec3 T = normalize(tTangent);
    vec3 B = normalize(cross(T, N));
    mat3 tbn = mat3(T, B, N);

    // material
    vec3 Albedo = uUseAlbedo ? texture(uTexAlbedo, tTexCoord).rgb : textureLod(uTexAlbedo, tTexCoord, 10).rgb;
    vec3 NormalMap = uUseNormal ? texture(uTexNormal, tTexCoord).rgb : vec3(0.5,0.5,1);
    float AO = uUseAO ? texture(uTexAO, tTexCoord).r : textureLod(uTexAO, tTexCoord, 10).r;
    float Height = uUseHeight ? texture(uTexHeight, tTexCoord).r : textureLod(uTexHeight, tTexCoord, 10).r;
    float Metallic = uUseMetallic ? texture(uTexMetallic, tTexCoord).r : textureLod(uTexMetallic, tTexCoord, 10).r;
    float Roughness = uUseRoughness ? texture(uTexRoughness, tTexCoord).r : textureLod(uTexRoughness, tTexCoord, 10).r;

    // normal mapping
    NormalMap.xy = NormalMap.xy * 2 - 1;
    N = normalize(tbn * NormalMap);

    // colors
    vec3 color = vec3(0.0);
    vec3 diffuse = Albedo * (1 - Metallic); // metals have no diffuse
    vec3 specular = mix(vec3(0.04), Albedo, Metallic); // fixed spec for non-metals

    // reflection
    float reflectivity = 0.05 * Metallic;
    float lod = Roughness * 15;
    vec3 reflection = textureLod(uCubeMap, R, lod).rgb;

    // calculate light source
    vec3 L = normalize(uLightPos - tWorldPos);
    vec3 lightColor = vec3(1);
    vec3 lightPos = uLightPos;

    // sphere area light
    {
        L = lightPos - tWorldPos;
        float origLdis = length(L);
        vec3 c2ray = dot(L, R) * R - L;
        lightPos += c2ray * clamp(uLightRadius / length(c2ray), 0.0, 1.0);
        L = normalize(lightPos - tWorldPos);

        float a = sqrt(max(Roughness, 0.001));
        float a2 = min(1.0, a + uLightRadius / (2 * origLdis));
        float areaNormalization = pow(a / a2, 2);
        lightColor *= areaNormalization;
    }

    // precalc dots
    float dotNL = dot(N, L);
    float dotRL = dot(R, L);

    // add light
    {
        // ambient
        vec3 ambient = vec3(0.04);
        color += ambient;

        // lambert
        color += lightColor * diffuse * max(0.0, dotNL);

        // ggx
        color += lightColor * shadingSpecularGGX(N, V, L, max(0.01, Roughness), specular);

        // skybox reflection
        if (uUseReflection)
            color += reflectivity * reflection;

        // ao
        color *= AO;
    }

    // gamma correction
    fColor = pow(color, vec3(1/2.2));

    // debug
    if (uShowTexture == 0) fColor = Albedo;
    else if (uShowTexture == 1) fColor = vec3(AO);
    else if (uShowTexture == 2) fColor = vec3(Height);
    else if (uShowTexture == 3) fColor = N;
    else if (uShowTexture == 4) fColor = vec3(Metallic);
    else if (uShowTexture == 5) fColor = vec3(Roughness);
    else if (uShowTexture == 6) fColor = reflection;
}
