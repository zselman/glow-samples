#pragma once

#include <glm/ext.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

namespace glow
{
namespace camera
{
class FixedCamera;
}
}

class GrassSample : public glow::glfw::GlfwApp
{
public:
    GrassSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShaderGround;
    glow::SharedProgram mShaderGrass;
    glow::SharedProgram mShaderGrassVoxelize;
    glow::SharedProgram mShaderVoxelResolve;
    glow::SharedProgram mShaderVoxelResolveDirs;
    glow::SharedProgram mShaderDebugVolume;
    glow::SharedProgram mShaderDrawTexture;
    glow::SharedProgram mShaderCubeTrace;
    glow::SharedVertexArray mCube;
    glow::SharedVertexArray mQuad;
    glow::SharedVertexArray mPlane;
    glow::SharedVertexArray mProtoGrass;

    glow::SharedTexture3D mTexGrassVolume;
    glow::SharedTexture3D mTexGrassVolumeDirs;

    glow::SharedTexture2D mTexDebugVolX;
    glow::SharedTexture2D mTexDebugVolY;
    glow::SharedTexture2D mTexDebugVolZ;

    bool mDrawProtoGeometry = false;
    bool mDrawProtoGeometry4 = true;
    bool mDrawDebugVolume = false;
    bool mDrawDebugVolumeTextures = false;
    bool mDrawCubeTrace = true;
    bool mDrawGround = true;

    bool mVoxelizeX = true;
    bool mVoxelizeY = true;
    bool mVoxelizeZ = true;

    int mVolumeSize = 64;
    float mGrassDepth = 0.005f;

    glm::vec3 mLightDir = normalize(glm::vec3(.2, 1, .3));
    glm::vec3 mSpecularColor = glm::vec3(0.3, 1.0, .2) * .7f;
    float mSpecularExponent = 8;

    void GenerateVolume();
    void drawTexture(int x, int y, int w, int h, glow::SharedTexture2D const& tex);

protected:
    void init() override;
    void render(float elapsedSeconds) override;

    void onRenderOpaquePass(glow::pipeline::RenderContext const& ctx) override;

    void onRenderTransparentPass(glow::pipeline::RenderContext const& ctx) override;
};
