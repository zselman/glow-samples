uniform vec2 uFrom;
uniform vec2 uTo;

in vec2 aPosition;

out vec2 vPosition;

void main() {
    vPosition = aPosition;
    gl_Position = vec4(mix(uFrom, uTo, aPosition) * 2 - 1, 0.0, 1.0);
}
