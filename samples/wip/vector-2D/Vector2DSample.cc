#include "Vector2DSample.hh"

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/vector/debug.hh>
#include <glow-extras/vector/graphics2D.hh>

#include <typed-geometry/tg.hh>

static std::string const sDataPath = glow::util::pathOf(__FILE__) + "/../../../data/";

void Vector2DSample::init()
{
    setGui(GlfwApp::Gui::ImGui);
    GlfwApp::init(); // call to base!

    // load fonts
    mVectorRenderer.loadFontFromFile("sans", sDataPath + "fonts/Roboto-Regular.ttf");
    mVectorRenderer.loadFontFromFile("sans-bold", sDataPath + "fonts/Roboto-Bold.ttf");

    mVectorRenderer.loadFontFromFile("fa", sDataPath + "fonts/Font-Awesome-5-Solid.otf");
    mVectorRenderer.loadFontFromFile("fa-regular", sDataPath + "fonts/Font-Awesome-5-Regular.otf");

    mVectorRenderer.addFallbackFont("sans", "fa"); // if not found in sans, use fa
}

void Vector2DSample::render(float elapsedSeconds)
{
    mRuntime += elapsedSeconds;
    auto time = mAnimate ? mRuntime : 0.0f;

    GLOW_SCOPED(clearColor, 1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // raw image
    {
        glow::vector::image2D img;
        auto ll = low_level_api(img);

        ll.begin_path();
        ll.rect(10, 30 + 25 * tg::sin(tg::radians(time * 3)), 40, 50);

        ll.set_fill_color(1, 0, 1, 1);
        ll.fill();

        ll.set_stroke_color(0, 0, 0, 1);
        ll.set_stroke_width(3);
        ll.stroke();

        ll.set_fill_color(0, 0, 0, 1);
        ll.set_font_face("sans");
        ll.set_font_size(20.f);
        ll.text(10, 250, "Hello World!");

        ll.set_font_face("fa");
        ll.text(10, 200, u8"\uf080 \uf126 \uf382 \uf6d5");
        ll.set_font_face("sans");
        ll.text(10, 225, u8"Hello World! \uf080");

        mVectorRenderer.render(img, getWindowWidth(), getWindowHeight());
    }

    // high level API
    {
        glow::vector::image2D img;
        auto g = graphics(img);

        // ways to specify a brush
        g.draw(tg::pos2(100, 100), tg::color3::blue);
        g.draw(tg::pos2(110, 100), tg::color4::blue);
        g.draw(tg::pos2(120, 100), {1, 0, 0});
        g.draw(tg::pos2(130, 100), {1, 0, 0, 1});
        g.draw(tg::pos2(140, 100), {tg::color3::blue, 5.0f});
        g.draw(tg::pos2(150, 100), {tg::color4::blue, 5.0f});
        g.draw(tg::pos2(160, 100), {{1, 0, 0}, 5.0f});
        g.draw(tg::pos2(170, 100), {{1, 0, 0, 1}, 5.0f});

        // ways to specify a fill
        g.fill(tg::aabb2({100, 120}, {150, 125}), tg::color3::green);
        g.fill(tg::aabb2({100, 130}, {150, 135}), tg::color4::green);
        g.fill(tg::aabb2({100, 140}, {150, 145}), {1, 0, 0});
        g.fill(tg::aabb2({100, 150}, {150, 155}), {1, 0, 0, 1});

        // line drawings
        g.draw(tg::segment2({110, 90}, {80, 80}), tg::color3::red);

        // fillings

        // other objects
        {
            float x = 250;
            float y = 100;
            auto next = [&] {
                x += 100;
                if (x > 1000)
                {
                    x = 250;
                    y += 100;
                }
            };

            g.fill(tg::aabb2({x - 20, y - 10}, {x + 20, y + 10}), tg::color3::green);
            g.draw(tg::aabb2({x - 20, y - 10}, {x + 20, y + 10}), {{0, 0, 0}, 3});
            next();

            g.fill(tg::triangle2({x - 20, y - 10}, {x + 10, y - 20}, {x, y + 20}), {0, 0, 1});
            g.draw(tg::triangle2({x - 20, y - 10}, {x + 10, y - 20}, {x, y + 20}), {{0, 0, 0}, 2});
            next();

            g.fill(tg::sphere2({x, y}, 15.f), {0, 0, 1});
            g.draw(tg::sphere2({x, y}, 15.f), {{0, 0, 0}, 2});
            next();

            g.fill(tg::circle2({x, y}, 15.f), {0, 0, 1});
            g.draw(tg::circle2({x, y}, 15.f), {{0, 0, 0}, 2});
            next();

            // paints
            g.fill(tg::aabb2({x - 20, y - 10}, {x + 20, y + 10}),
                   glow::vector::paint2D::linear_gradient({x - 10, y - 5}, {x + 10, y + 5}, tg::color4::red, tg::color4::blue));
            g.draw(tg::aabb2({x - 20, y - 10}, {x + 20, y + 10}), {0, 0, 0});
            next();

            g.fill(tg::aabb2({x - 20, y - 10}, {x + 20, y + 10}),
                   glow::vector::paint2D::radial_gradient({x - 5, y - 5}, 5, 15, tg::color4::red, tg::color4::blue));
            g.draw(tg::aabb2({x - 20, y - 10}, {x + 20, y + 10}), {0, 0, 0});
            next();

            g.fill(tg::aabb2({x - 40, y - 30}, {x + 40, y + 30}),
                   glow::vector::paint2D::box_gradient({{x - 25, y - 15}, {x + 25, y + 15}}, 5, 8, tg::color4::red, tg::color4::blue));
            g.draw(tg::aabb2({x - 40, y - 30}, {x + 40, y + 30}), {0, 0, 0});
            next();

            // text
            g.text({x, y}, "I am Text.", "sans");
            next();

            g.text({x, y}, "I am Centered.", {"sans", 14, glow::vector::text_align::middle_center});
            next();

            g.text_box({x, y}, 100, "I is automatically wrapped around.", {"sans", 18, glow::vector::text_align::middle_center});
            next();

            g.text_box({x, y}, 100, "I has\nnew lines\nin me.", {"sans", 18, glow::vector::text_align::middle_center});
            next();

            g.text_box({x, y}, 100, u8"\uf080 \uf126 \uf382 \uf6d5", {"fa", 28, glow::vector::text_align::middle_center});
            next();
        }

        // TODO: more

        mVectorRenderer.render(img, getWindowWidth(), getWindowHeight());
    }
}
