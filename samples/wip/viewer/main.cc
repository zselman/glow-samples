#include <random>

#include <imgui/imgui.h>

#include <glow/common/str_utils.hh>
#include <glow/data/TextureData.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/util/AsyncTextureLoader.hh>

#include <polymesh/algorithms/normalize.hh>
#include <polymesh/formats.hh>
#include <polymesh/objects/quad.hh>

#include <glow-extras/glfw/GlfwContext.hh>
#include <glow-extras/vector/graphics2D.hh>
#include <glow-extras/vector/image2D.hh>
#include <glow-extras/viewer/view.hh>

#include <typed-geometry/tg.hh>

int main()
{
    std::string const dataPath = glow::util::pathOf(__FILE__) + "/../../../data/";

    // Create a context
    glow::glfw::GlfwContext ctx;

    std::uniform_real_distribution<float> dis(0.0f, 1.0f);
    std::default_random_engine rng;

    // Load a polymesh mesh
    pm::Mesh m;
    auto pos = m.vertices().make_attribute<tg::pos3>();
    load(dataPath + "suzanne.obj", m, pos);
    normalize(pos); // make it -1..1

    // prepare some data
    pm::vertex_attribute<glm::vec2> uv = pos.map([](tg::pos3 v) { return glm::vec2(v.x, v.y); });
    pm::vertex_attribute<float> vdata = pos.map([](tg::pos3 v) { return v.y; });
    pm::vertex_attribute<tg::vec3> vnormals = vertex_normals_by_area(pos);
    pm::face_attribute<float> fdata = m.faces().map([&](pm::face_handle f) { return f.vertices().avg(pos).z; });
    glow::SharedTexture2D tex = glow::Texture2D::createFromFile(dataPath + "textures/tiles.color.png", glow::ColorSpace::sRGB);
    glow::AsyncTexture2D atex = glow::AsyncTextureLoader::load2D(dataPath + "textures/tiles.color.png", glow::ColorSpace::sRGB);
    pm::face_attribute<glow::colors::color> fcolors = m.faces().map([&](pm::face_handle) { return glow::colors::color(dis(rng), dis(rng), dis(rng)); });
    pm::vertex_attribute<glow::colors::color> vcolors = attribute(m.vertices(), glow::colors::color(1, 0, 0));
    pm::edge_attribute<float> edge_lengths = m.edges().map([&](pm::edge_handle e) { return edge_length(e, pos); });
    pm::vertex_attribute<float> ptsize = m.vertices().map([&](pm::vertex_handle v) { return v.edges().avg(edge_lengths); });

    // Simplest view
    glow::viewer::view(pos);

    // Grid of examples
    {
        auto v = glow::viewer::grid();

        // Scoped configuration
        {
            GLOW_VIEWER_CONFIG(glow::viewer::no_grid);

            // ADL view
            view(pos);
        }

        // named view
        view(pos, "Suzanne");

        // smooth normals
        view(polygons(pos).smooth_normals(), "smoothed normals");

        // Configuration nesting
        {
            GLOW_VIEWER_CONFIG(glow::viewer::print_mode);

            // Colored faces
            view(pos, fcolors, "colored faces");

            {
                GLOW_VIEWER_CONFIG(glow::viewer::no_shadow);

                // Colored vertices
                view(pos, vcolors, "colored vertices");
            }
        }

        // Mapped 1D vertex data
        view(pos, mapping(vdata).linear({0, 0, 0}, {1, 0, 0}, 0.1f, 0.3f), "vertex data");

        // Mapped 1D face data
        view(pos, mapping(fdata).linear({0, 1, 0}, {0, 0, 1}, -0.5f, 0.5f).clamped(), "face data");

        // Textured mesh
        view(pos, textured(uv, tex), "textured");

        // Textured mesh (async texture)
        view(pos, textured(uv, atex), "textured (async)");

        // Simple point cloud
        view(points(pos), "point cloud");

        // Square, oriented point cloud with adaptive point size
        view(points(pos).point_size_world(ptsize).normals(vnormals).square(), "normal oriented squares");

        // Points rendered as 3D spheres
        view(points(pos).spheres().point_size_world(0.03f), "sphere cloud");

        // Simple line soup
        view(lines(pos), "edges");

        // Configure view
        {
            auto v = glow::viewer::view();
            v.view(pos, glow::colors::color(0, 1, 0), "configured view");
            // when out-of-scope, v immediately shows
        }

        // Multiple objects
        {
            auto v = glow::viewer::view();
            v.view(polygons(pos).move({-1.5f, 0, 0}), glow::colors::color(1, 0, 0), "multiple objects");
            v.view(polygons(pos).move({+1.5f, 0, 0}), glow::colors::color(0, 0, 1));

            // or:
            v.view(pos, scaling(tg::size3(0.5f, 1, 1.5f)), glow::colors::color(0, 1, 0));
        }

        // transparencies
        view(pos, tg::color4(0, 0.4f, 0.3f, 0.2f), "transparency with fresnel");
        view(pos, tg::color4(0, 0.4f, 0.3f, 0.2f), glow::viewer::no_fresnel, "transparency without fresnel");

        // Complex material
        // TODO

        // Text
        // TODO

        // Vector Graphics
        // see Vector2DSample for more sample code
        {
            glow::vector::image2D img;
            auto g = graphics(img);
            g.fill(tg::circle2({100, 100}, 70), tg::color3::red);
            g.draw(tg::circle2({100, 100}, 70), {tg::color3::black, 2});
            view(img);
        }

        // Images
        // TODO

        // Scene setup
        // TODO

        // Multiple views
        {
            auto v = glow::viewer::columns();
            glow::viewer::view(pos, glow::colors::color(0, 0, 1)); // attaches to columns view
            v.view(pos, glow::colors::color(0, 0, 1));             // explicitly adds view
            {
                auto r = v.rows();
                r.view(pos, glow::colors::color(0, 1, 1));
                r.view(pos, glow::colors::color(1, 1, 0));
                // when out-of-scope, r attaches to v
            }
            // when out-of-scope, v immediately shows
        }

        // Picking
        {
            // TODO
        }
    }

    // Interactive
    {
        auto v = glow::viewer::grid();

        // Non-interactive
        glow::viewer::view(pos);

        // Interactive
        {
            // Creating renderables is expensive, cache them whenever possible, and capture by value!
            auto r1 = glow::viewer::make_renderable(pos);
            glow::viewer::interactive([r1](auto dt) {
                static auto time = 0.f;
                time += dt;

                // Always a cleared view, resetting accumulation each frame
                glow::viewer::view_cleared(r1, tg::translation(tg::vec3(tg::sin(tg::radians(time * .5f)) * .5f, 0.f, 0.f)));
            });

            // Using imgui in an interactive view
            auto r2 = glow::viewer::make_renderable(pos);
            glow::viewer::interactive([r2](auto) {
                static float configurable = 0.f;

                auto const input = ImGui::SliderFloat("Height", &configurable, -3.f, 3.f);

                // conditionally clear view if input has changed
                glow::viewer::view(r2, tg::translation(tg::vec3(0.f, configurable, 0.f)), glow::viewer::clear_accumulation(input));
            });
        }
    }

    // Create a "headless" screenshot
    {
        // Viewer never shows a window, returns once screenshot is rendered
        GLOW_VIEWER_CONFIG(glow::viewer::headless_screenshot(tg::ivec2(1000, 1000), 32, "demo_screenshot.png"));
        glow::viewer::view(pos);
    }

    // Conditional RAII objects
    {
        // Use an outer viewer object conditionally
        auto o = (sizeof(int) > 1) ? glow::viewer::rows() : glow::viewer::nothing();

        {
            // Camera starting position
            GLOW_VIEWER_CONFIG(glow::viewer::camera_orientation(125_deg, -15_deg, 1.7f));

            // Tonemapping
            GLOW_VIEWER_CONFIG(glow::viewer::tonemap_exposure(3.5f));

            // These two either nest into the outer object, or create their own windows
            view(pos, textured(uv, tex));
            view(points(pos));
        }
    }

    // tg objects
    {
        auto v = glow::viewer::grid();
        tg::rng rng;

        view(tg::segment3(tg::pos3(0, 0, 0), tg::pos3(1, 0, 0)), tg::color3::red);
        view(tg::triangle3({0, 0, 0}, {1, 0, 0}, {0, 1, 0}), tg::color3::blue);
        view(tg::aabb3({-0.3f, 0, -0.4f}, {0.2f, 0.5f, 0.1f}), tg::color3::green);
        view(lines(tg::aabb3({-0.3f, 0, -0.4f}, {0.2f, 0.5f, 0.1f})));

        // vector versions
        {
            std::vector<tg::segment3> segs;
            for (auto i = 0; i < 20; ++i)
                segs.emplace_back(uniform(rng, tg::sphere3::unit), uniform(rng, tg::sphere3::unit));
            view(segs);
        }
        {
            std::vector<tg::triangle3> tris;
            for (auto i = 0; i < 20; ++i)
                tris.emplace_back(uniform(rng, tg::sphere3::unit), uniform(rng, tg::sphere3::unit), uniform(rng, tg::sphere3::unit));
            view(tris);
        }
        {
            std::vector<tg::pos3> pts;
            for (auto i = 0; i < 500; ++i)
                pts.push_back(uniform(rng, tg::sphere3::unit));
            view(pts);
        }

        {
            std::vector<tg::box3> bbs;
            for (auto i = 0; i < 4; ++i)
            {
                auto e0 = uniform_vec(rng, tg::sphere3::unit);
                auto e1 = cross(e0, uniform_vec(rng, tg::sphere3::unit));
                auto e2 = cross(e1, e0);

                auto c = uniform(rng, tg::ball3::unit);

                e0 = normalize(e0) * uniform(rng, 0.2f, 0.5f);
                e1 = normalize(e1) * uniform(rng, 0.2f, 0.5f);
                e2 = normalize(e2) * uniform(rng, 0.2f, 0.5f);
                bbs.push_back(tg::box3(c, {e0, e1, e2}));
            }

            view(lines(bbs));
            view(bbs);
        }
    }

    // Interactive Torus
    {
        pm::Mesh m;

        auto pos = m.vertices().make_attribute<tg::pos3>();
        auto uv = m.vertices().make_attribute<glm::vec2>();
        pm::objects::add_quad(m,
                              [&](pm::vertex_handle v, float x, float y) {
                                  auto [cx, sx] = tg::sin_cos(tg::pi<float> * 2 * x);
                                  auto [cy, sy] = tg::sin_cos(tg::pi<float> * 2 * y);
                                  auto orad = 8.f;
                                  auto irad = 3.f;
                                  tg::vec3 t;
                                  t.x = cx;
                                  t.z = sx;
                                  tg::pos3 p;
                                  p.x = orad * cx;
                                  p.y = irad * cy;
                                  p.z = orad * sx;
                                  p += t * irad * sy;
                                  pos[v] = p;
                                  uv[v] = {1 - x, y};
                              },
                              32, 32);

        auto a = 0.f;
        auto animate = false;
        glow::viewer::interactive([&](auto dt) {
            ImGui::Begin("Torus");
            auto changed = ImGui::SliderFloat("angle", &a, 0.f, 360.f);
            ImGui::Checkbox("Animate", &animate);
            ImGui::End();

            if (animate)
                a += 5 * dt;
            changed |= animate;

            view(pos, glow::viewer::textured(uv, tex).transform(tg::rotation_around(tg::pos2::zero, tg::degree(a))), glow::viewer::clear_accumulation(changed));
        });
    }

    return 0;
}
