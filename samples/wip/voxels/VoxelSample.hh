#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

#include <glow-extras/debugging/DebugRenderer.hh>

#include "Voxels.hh"

class VoxelSample : public glow::glfw::GlfwApp
{
public:
    VoxelSample() : GlfwApp(Gui::AntTweakBar), mVoxels(this) {}

private:
    glm::vec3 mLightDir = glm::normalize(glm::vec3(0.2, 1, -0.1));

    Voxels mVoxels;
    glow::SharedProgram mShader;
    glow::SharedVertexArray mVoxelVAO;
    glow::SharedShaderStorageBuffer mVoxelBuffer;

    bool mUseOcclusion = true;
    bool mUseLighting = true;
    bool mRenderVoxels = true;

    glm::vec3 mSelectedPos;
    glm::ivec3 mSelectedCube = {-1, -1, -1};

    int mDebugLightDir = -1;
    int mDebugLightLevel = 0;

    glow::debugging::SharedDebugRenderer mDebugLight;

public:
    GLOW_GETTER(SelectedPos);
    GLOW_GETTER(SelectedCube);

private:
    void refresh();

protected:
    void init() override;
    void update(float elapsedSeconds) override;

    void onRenderOpaquePass(glow::pipeline::RenderContext const& ctx) override;

    // GlfwApp interface
protected:
    bool onMouseButton(double x, double y, int button, int action, int mods, int clickCount) override;
};
