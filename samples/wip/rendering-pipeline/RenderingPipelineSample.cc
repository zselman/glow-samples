#include "RenderingPipelineSample.hh"

#include <cmath>
#include <memory>
#include <vector>

#include <typed-geometry/tg.hh>

#include <imgui/imgui.h>

#include <glow/common/str_utils.hh>
#include <glow/data/TextureData.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/objects/VertexArray.hh>
#include <glow/util/DefaultShaderParser.hh>

#include <glow-extras/assimp/Importer.hh>
#include <glow-extras/colors/color.hh>
#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/geometry/Quad.hh>
#include <glow-extras/material/IBL.hh>
#include <glow-extras/pipeline/RenderScene.hh>
#include <glow-extras/pipeline/lights/Light.hh>
#include <glow-extras/pipeline/stages/StageCamera.hh>

using namespace glow;
using namespace glow::camera;
using namespace glow::pipeline;

constexpr float RenderingPipelineSample::sampleRoughnessDist[];
constexpr float RenderingPipelineSample::sampleMetallicDist[];

void RenderingPipelineSample::onResize(int w, int h) { getCamera()->setViewportSize(w, h); }

void RenderingPipelineSample::onGui()
{
    ImGui::SetNextWindowPos(ImVec2(390, 20), ImGuiCond_FirstUseEver);
    ImGui::SetNextWindowSize(ImVec2(200, 200), ImGuiCond_FirstUseEver);

    ImGui::Begin("Pipeline Sample");

    ImGui::Checkbox("Light coverage heatmap", &mDebugShowClusterHeatmap);
    ImGui::Checkbox("Shadow cascade colors", &mDebugShowShadowCascades);
    ImGui::Checkbox("Debug cluster assignment", &mDebugShowClusterCoords);
    ImGui::Checkbox("Debug shadow position", &mDebugShowShadowPosition);

    static bool rejectionEnabled = getPipelineCamera()->isRejectingHistory();
    ImGui::Checkbox("Reject TAA History", &rejectionEnabled);
    getPipelineCamera()->setRejectHistory(rejectionEnabled);

    //    if (ImGui::Button("Redraw shadows"))
    //    {
    //        getPipelineScene()->shadowMode = ShadowMode::UpdateOnce;
    //    }

    if (ImGui::Button("Reallocate targets"))
    {
        getPipeline()->freeAllTargets();
    }

    if (ImGui::Button("Reset Camera"))
    {
        getCamera()->setLookAt(tg::pos3(sampleMaxX - 4, 5, sampleMaxZ + 10), tg::pos3(sampleMaxX / 2, -3, sampleMaxZ / 2));
    }

    if (ImGui::Button("Toggle Fullscreen"))
        toggleFullscreen();

    ImGui::End();
}

void RenderingPipelineSample::onPerformShadowPass(const RenderScene&, const RenderStage&, const CameraData& d, UsedProgram& shader)
{
    drawOpaqueGeometry(shader, d, true);
}

void RenderingPipelineSample::onRenderDepthPrePass(glow::pipeline::RenderContext const& info)
{
    auto shader = info.useProgram(mShaderDepthPre);

    shader.setUniform("uView", info.camData.view);
    shader.setUniform("uProj", info.camData.proj);

    drawOpaqueGeometry(shader, info.camData, true);
}

void RenderingPipelineSample::onRenderOpaquePass(glow::pipeline::RenderContext const& info)
{
    auto shader = info.useProgram(mShaderOpaqueForward);

    shader.setUniform("uView", info.camData.view);
    shader.setUniform("uProj", info.camData.proj);
    shader.setUniform("uCamPos", info.camData.camPos);
    material::IBL::prepareShaderGGX(shader, mEnvironmentMap);

    shader.setUniform("uSunDirection", info.scene.sun.direction);
    shader.setUniform("uSunColor", info.scene.sun.color * info.scene.sun.intensity);

    // Debug
    shader.setUniform("uShowClusterHeatmap", mDebugShowClusterHeatmap);
    shader.setUniform("uShowClusterIndexHash", mDebugShowClusterCoords);
    shader.setUniform("uShowShadowCascades", mDebugShowShadowCascades);
    shader.setUniform("uShowShadowPosition", mDebugShowShadowPosition);

    drawOpaqueGeometry(shader, info.camData, false);
}

void RenderingPipelineSample::onGatherLights(glow::pipeline::LightCallback& lc)
{
    static constexpr auto radius = 7.f;
    static constexpr auto size = 1.5f;
    static constexpr auto defY = 3.5f;

    static constexpr auto intensity = 1.5f;
    static constexpr auto roughIntensity = 4.f;
    static const auto backLeftC = tg::color3(colors::color::from_hex("CEF19E").to_rgb());
    static const auto frontLeftC = tg::color3(colors::color::from_hex("836890").to_rgb());
    static const auto centerC = tg::color3(colors::color::from_hex("0DFF9F").to_rgb());
    static const auto backRightC = tg::color3(colors::color::from_hex("068587").to_rgb());
    static const auto frontRightC = tg::color3(colors::color::from_hex("ED553B").to_rgb());
    static const auto movingC = tg::color3(colors::color::from_hex("ED553B").to_rgb());

    lc.addLight(tg::pos3(1, defY, 1), backLeftC * intensity, size, radius);
    lc.addLight(tg::pos3(1, defY, sampleMaxZ - 1), frontLeftC * intensity, size, radius);
    lc.addLight(tg::pos3(sampleMaxX - 1, defY, 1), backRightC * roughIntensity, size, radius);
    lc.addLight(tg::pos3(sampleMaxX / 2, defY + 2, sampleMaxZ / 2), centerC * roughIntensity, size, radius);
    lc.addLight(tg::pos3(sampleMaxX - 1, defY, sampleMaxZ - 1), frontRightC * roughIntensity, size, radius);

    // Moving Light
    const auto timeVaryingX = (sampleMaxX / 2) + std::sin(getCurrentTime()) * (sampleMaxX / 2);
    const auto timeVaryingZ = (sampleMaxZ / 2) + std::cos(getCurrentTime()) * (sampleMaxZ / 2);
    lc.addLight(tg::pos3(timeVaryingX, defY, timeVaryingZ), movingC * intensity, size, 4.f);

    // Long Tube lights
    static constexpr auto tubeLightY = -7;
    lc.addLight(tg::pos3(1, tubeLightY, sampleMaxZ - 1), tg::pos3(sampleMaxX - 1, tubeLightY, 1), frontLeftC, .25f, radius);
    lc.addLight(tg::pos3(1, tubeLightY, 1), tg::pos3(sampleMaxX - 1, tubeLightY, sampleMaxZ - 1), backRightC, .25f, radius);
}

void RenderingPipelineSample::onRenderTransparentPass(glow::pipeline::RenderContext const& info)
{
    auto shader = info.useProgram(mShaderTransparent);

    shader.setUniform("uView", info.camData.view);
    shader.setUniform("uProj", info.camData.proj);
    shader.setUniform("uCamPos", info.camData.camPos);
    shader.setUniform("uSunDirection", info.scene.sun.direction);
    shader.setUniform("uSunColor", info.scene.sun.color);

    for (auto const& mesh : mTransparentMeshEntries)
    {
        shader.setUniform("uModel", mesh.model);

        shader.setUniform("uRoughness", mesh.roughness);
        shader.setUniform("uMetallic", mesh.metallic);
        shader.setUniform("uAlbedo", mesh.albedo);
        shader.setUniform("uMaterialAlpha", mesh.alpha);

        mMeshSample->bind().draw();
    }
}

void RenderingPipelineSample::initMeshes()
{
    // Monkeys
    int roughnessCount = 0;
    for (float roughness : sampleRoughnessDist)
    {
        int metallicCount = 0;
        for (float metallic : sampleMetallicDist)
        {
            mMeshEntries.emplace_back(MeshEntry{
                mMeshSample, tg::translation(tg::vec3(roughnessCount * sampleRoughnessPosOffset, 0, metallicCount * sampleMetallicPosOffset)),
                tg::color3(1, .9f, 1), roughness, metallic});

            mTransparentMeshEntries.emplace_back(
                TransparentMeshEntry{tg::translation(tg::vec3(roughnessCount * sampleRoughnessPosOffset, -4, metallicCount * sampleMetallicPosOffset)),
                                     tg::color3(.6f, .6f, 1), roughness, metallic, std::max(roughness, 0.1f)});

            ++metallicCount;
        }
        ++roughnessCount;
    }

    // Floor
    mMeshEntries.emplace_back(MeshEntry{glow::geometry::Cube<>().generate(),
                                        tg::translation(tg::vec3(sampleMaxX / 2, -8, sampleMaxZ / 2)) * tg::scaling(tg::size3(sampleMaxX, .25f, sampleMaxZ)),
                                        tg::color3(.75), .45f, .95f});
}

void RenderingPipelineSample::drawOpaqueGeometry(UsedProgram& shader, const CameraData& camData, bool zPreOnly)
{
    for (auto const& mesh : mMeshEntries)
    {
        shader.setUniform("uModel", mesh.model);

        if (!zPreOnly)
        {
            const auto cleanMvp = camData.cleanVp * mesh.model;
            const auto prevCleanMvp = camData.prevCleanVp * mesh.model; // Note that this would have to be previousModel if the objects moved


            shader.setUniform("uCleanMvp", cleanMvp);
            shader.setUniform("uPrevCleanMvp", prevCleanMvp);

            shader.setUniform("uRoughness", mesh.roughness);
            shader.setUniform("uMetallic", mesh.metallic);
            shader.setUniform("uAlbedo", mesh.albedo);
        }

        mesh.mesh->bind().draw();
    }
}

void RenderingPipelineSample::init()
{
    // -- GlfwApp Init --
    {
        setUsePipeline(true);
        setGui(GlfwApp::Gui::ImGui);
        setUsePipelineConfigGui(true);
        setCacheWindowSize(true);

        GlfwApp::init();
    }

    // -- Shader Include Setup --
    {
        // Initialize Material shaders
        glow::material::IBL::GlobalInit();
    }

    // -- Sample-specific configuration --
    {
        getPipelineScene()->sun.intensity = .15f;
        getPipelineScene()->contrast = 1.3f;
        getCamera()->setFarPlane(2000);
        getCamera()->setNearPlane(0.01f);
        getCamera()->setLookAt(tg::pos3(sampleMaxX - 4, 5, sampleMaxZ + 10), tg::pos3(sampleMaxX / 2, -3, sampleMaxZ / 2));
    }

    // -- Asset loading --
    {
        DefaultShaderParser::addIncludePath(util::pathOf(__FILE__) + "/shaders");

        mShaderDepthPre = Program::createFromFiles({"depthPre/geometry.vsh", "depthPre/depthPre.fsh"});
        mShaderOpaqueForward = Program::createFromFiles({"geometry.vsh", "opaque/litOpaque.fsh"});
        mShaderTransparent = Program::createFromFiles({"geometry.vsh", "transparent/oit.fsh"});

        const std::string dataPath = util::pathOf(__FILE__) + "/../../../data/";

        mMeshSample = assimp::Importer().load(dataPath + "suzanne.obj");

        const auto skyboxPath = dataPath + "ibl/studio/skybox/skybox_";
        mSkyboxMap = TextureCubeMap::createFromData(TextureData::createFromFileCube(skyboxPath + "posx.hdr", skyboxPath + "negx.hdr",
                                                                                    skyboxPath + "posy.hdr", skyboxPath + "negy.hdr",
                                                                                    skyboxPath + "posz.hdr", skyboxPath + "negz.hdr", ColorSpace::sRGB));
    }

    // -- IBL setup --
    {
        mEnvironmentMap = material::IBL::createEnvMapGGX(mSkyboxMap, 1024);

        auto usedShader = mShaderOpaqueForward->use();
        material::IBL::initShaderGGX(usedShader);
    }

    // -- Sample scene init --
    {
        initMeshes();
    }
}

void RenderingPipelineSample::render(float dt) { GlfwApp::render(dt); }
