#include <doctest.hh>

#include <glow/std140.hh>
#if !GLOW_COMPILER_MSVC
typedef tg::vec4 std140vec4b __attribute__((__aligned__(4 * 4)));
typedef tg::vec4 std140vec4c __attribute__((__aligned__(4 * 4)));
typedef tg::vec3 std140vec3c __attribute__((__aligned__(4 * 4)));
#endif

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/UniformBuffer.hh>
#if !GLOW_COMPILER_MSVC
typedef tg::vec4 std140vec4d __attribute__((__aligned__(4 * 4)));
typedef tg::vec3 std140vec3b __attribute__((__aligned__(4 * 4)));
#endif

using namespace glow;

namespace
{
struct MyBuffer
{
    // basic types
    std140bool i = true;
    std140int j = -3;
    std140uint k = 9;
    std140float l = 1.f;

    // vectors
    std140vec2 v2;
    std140vec3 v3;
    std140vec4 v4;

    std140ivec2 iv2;
    std140ivec3 iv3;
    std140ivec4 iv4;

    std140dvec2 dv2;
    std140dvec3 dv3;
    std140dvec4 dv4;

    std140uvec2 uv2;
    std140uvec3 uv3;
    std140uvec4 uv4;

    // matrices
    std140mat4 m4;

    std140mat2x4 m2x4;
    std140mat3x4 m3x4;
    std140mat4x4 m4x4;

    std140dmat2 dm2;
    std140dmat4 dm4;

    std140dmat2x2 dm2x2;
    std140dmat2x4 dm2x4;
    std140dmat3x2 dm3x2;
    std140dmat3x4 dm3x4;
    std140dmat4x2 dm4x2;
    std140dmat4x4 dm4x4;
};
}

TEST_CASE("Std140, Access")
{
    MyBuffer b;

    bool i = b.i;
    b.i = !i;
    CHECK(b.i == false);

    int j = b.j;
    b.j = j + 1;
    CHECK(b.j == -2);

    unsigned k = b.k;
    b.k = k + 1;
    CHECK(b.k == 10u);

    float l = b.l;
    b.l = l + 1;
    CHECK(b.l == 2.f);

    tg::vec2 v2 = b.v2;
    b.v2 = v2;
    tg::vec3 v3 = b.v3;
    b.v3 = v3;
    tg::vec4 v4 = b.v4;
    b.v4 = v4;

    tg::ivec2 iv2 = b.iv2;
    b.iv2 = iv2;
    tg::ivec3 iv3 = b.iv3;
    b.iv3 = iv3;
    tg::ivec4 iv4 = b.iv4;
    b.iv4 = iv4;

    tg::dvec2 dv2 = b.dv2;
    b.dv2 = dv2;
    tg::dvec3 dv3 = b.dv3;
    b.dv3 = dv3;
    tg::dvec4 dv4 = b.dv4;
    b.dv4 = dv4;

    tg::uvec2 uv2 = b.uv2;
    b.uv2 = uv2;
    tg::uvec3 uv3 = b.uv3;
    b.uv3 = uv3;
    tg::uvec4 uv4 = b.uv4;
    b.uv4 = uv4;
}

#if !GLOW_COMPILER_MSVC
TEST_CASE("Std140, Vec3")
{
    struct v3a
    {
        float a, b, c;
    };
    struct v3b
    {
        float a __attribute__((aligned(4 * 4)));
        float b;
        float c;
    };
    typedef v3a v3c __attribute__((aligned(4 * 4)));
    typedef glm::vec3 v3d __attribute__((aligned(4 * 4)));
    typedef glm::vec4 v4d __attribute__((aligned(4 * 4)));
    typedef float v1d __attribute__((aligned(1 * 4)));
    typedef glm::ivec3 iv3d __attribute__((aligned(4 * 4)));

    struct ta
    {
        v3a a;
        float b;
        v3a c;
        v3a d;
    };
    struct tc
    {
        v3c a;
        float b;
        v3c c;
        v3c d;
    };
    struct td
    {
        v3d a;
        float b;
        v3d c;
        v3d d;
    };
    struct itd
    {
        iv3d a;
        float b;
        iv3d c;
        iv3d d;
    };
    struct te
    {
        std140vec3c a;
        float b;
        std140vec3c c;
        std140vec3c d;
    };
    struct tf
    {
        std140vec3 a;
        float b;
        std140vec3 c;
        std140vec3 d;
    };
    struct itf
    {
        std140ivec3 a;
        float b;
        std140ivec3 c;
        std140ivec3 d;
    };

    CHECK(sizeof(v3a) == 3 * sizeof(float));
    CHECK(offsetof(ta, a) == 0 * sizeof(float));
    CHECK(offsetof(ta, b) == 3 * sizeof(float));
    CHECK(offsetof(ta, c) == 4 * sizeof(float));
    CHECK(offsetof(ta, d) == 7 * sizeof(float));

    // CHECK(sizeof(v3b) == 3 * sizeof(float));

    CHECK(sizeof(v3c) == 3 * sizeof(float));
    CHECK(offsetof(tc, a) == 0 * sizeof(float));
    CHECK(offsetof(tc, b) == 3 * sizeof(float));
    CHECK(offsetof(tc, c) == 4 * sizeof(float));
    CHECK(offsetof(tc, d) == 8 * sizeof(float));

    CHECK(sizeof(v3d) == 3 * sizeof(float));
    CHECK(offsetof(td, a) == 0 * sizeof(float));
    CHECK(offsetof(td, b) == 3 * sizeof(float));
    CHECK(offsetof(td, c) == 4 * sizeof(float));
    CHECK(offsetof(td, d) == 8 * sizeof(float));

    CHECK(sizeof(iv3d) == 3 * sizeof(float));
    CHECK(offsetof(itd, a) == 0 * sizeof(float));
    CHECK(offsetof(itd, b) == 3 * sizeof(float));
    CHECK(offsetof(itd, c) == 4 * sizeof(float));
    CHECK(offsetof(itd, d) == 8 * sizeof(float));

    CHECK(sizeof(std140vec3) == 3 * sizeof(float));
    CHECK(offsetof(te, a) == 0 * sizeof(float));
    CHECK(offsetof(te, b) == 3 * sizeof(float));
    CHECK(offsetof(te, c) == 4 * sizeof(float));
    CHECK(offsetof(te, d) == 8 * sizeof(float));

    CHECK(sizeof(std140vec3) == 3 * sizeof(float));
    CHECK(offsetof(tf, a) == 0 * sizeof(float));
    CHECK(offsetof(tf, b) == 3 * sizeof(float));
    CHECK(offsetof(tf, c) == 4 * sizeof(float));
    CHECK(offsetof(tf, d) == 8 * sizeof(float));

    CHECK(sizeof(std140vec3) == 3 * sizeof(float));
    CHECK(offsetof(itf, a) == 0 * sizeof(float));
    CHECK(offsetof(itf, b) == 3 * sizeof(float));
    CHECK(offsetof(itf, c) == 4 * sizeof(float));
    CHECK(offsetof(itf, d) == 8 * sizeof(float));

    struct t4a
    {
        float a;
        v4d b;
    };
    CHECK(sizeof(v4d) == 4 * sizeof(float));
    CHECK(offsetof(t4a, a) == 0 * sizeof(float));
    CHECK(offsetof(t4a, b) == 4 * sizeof(float));

    struct t4b
    {
        v1d a;
        v4d b;
    };
    CHECK(sizeof(v1d) == 1 * sizeof(float));
    CHECK(sizeof(v4d) == 4 * sizeof(float));
    CHECK(offsetof(t4b, a) == 0 * sizeof(float));
    CHECK(offsetof(t4b, b) == 4 * sizeof(float));
}

typedef glm::vec4 v4eUB1 __attribute__((aligned(4 * 4)));

TEST_CASE("Std140, UB1")
{
    struct TestUB1
    {
        std140float a;
        std140vec4 b;
        std140vec3 c;
        std140bool d;
        std140vec2 e;
    };
    typedef glm::vec4 v4d __attribute__((__aligned__(4 * 4)));
    struct TestUB1b
    {
        std140float a;
        std140vec4 b;
    };
    struct TestUB1c
    {
        std140float a;
        v4d b;
    };
    struct TestUB1d
    {
        std140float a;
        v4eUB1 b;
    };
    struct TestUB1e
    {
        std140float a;
        std140vec4b b;
    };
    struct TestUB1f
    {
        std140float a;
        std140vec4c b;
    };
    struct TestUB1g
    {
        std140float a;
        std140vec4d b;
    };

    CHECK(offsetof(TestUB1g, a) == 0 * sizeof(float));
    CHECK(offsetof(TestUB1g, b) == 4 * sizeof(float));

    CHECK(offsetof(TestUB1f, a) == 0 * sizeof(float));
    CHECK(offsetof(TestUB1f, b) == 4 * sizeof(float));

    CHECK(offsetof(TestUB1e, a) == 0 * sizeof(float));
    CHECK(offsetof(TestUB1e, b) == 4 * sizeof(float));

    CHECK(offsetof(TestUB1c, a) == 0 * sizeof(float));
    CHECK(offsetof(TestUB1c, b) == 4 * sizeof(float));

    CHECK(offsetof(TestUB1d, a) == 0 * sizeof(float));
    CHECK(offsetof(TestUB1d, b) == 4 * sizeof(float));

    CHECK(offsetof(TestUB1b, a) == 0 * sizeof(float));
    CHECK(offsetof(TestUB1b, b) == 4 * sizeof(float));

    CHECK(offsetof(TestUB1, a) == 0 * sizeof(float));
    CHECK(offsetof(TestUB1, b) == 4 * sizeof(float));

    auto ub = UniformBuffer::create();
    ub->addVerification({
        {&TestUB1::a, "a"}, //
        {&TestUB1::b, "b"}, //
        {&TestUB1::c, "c"}, //
        {&TestUB1::d, "d"}, //
        {&TestUB1::e, "e"},
    });
    auto shader = Shader::createFromSource(GL_VERTEX_SHADER, "layout(std140) uniform Buffer { "
                                                             "float a; vec4 b; vec3 c; bool d; vec2 e;"
                                                             "};"
                                                             "out float x; void main() { x = a + b.x + c.x + float(d) + e.x; }");
    auto prog = Program::create(shader);
    CHECK(prog->verifyUniformBuffer("Buffer", ub));
}
#endif

TEST_CASE("Std140, UB2")
{
    struct TestUB2
    {
        std140vec3 a;
        std140float b;
        std140vec3 c;
        std140vec3 d;
        std140vec3 e;
    };
    CHECK(sizeof(std140vec3) == 3 * sizeof(float));

    auto ub = UniformBuffer::create();
    ub->addVerification({
        {&TestUB2::a, "a"}, //
        {&TestUB2::b, "b"}, //
        {&TestUB2::c, "c"}, //
        {&TestUB2::d, "d"}, //
        {&TestUB2::e, "e"},
    });
    auto shader = Shader::createFromSource(GL_VERTEX_SHADER, "layout(std140) uniform Buffer { "
                                                             "vec3 a; float b; vec3 c; vec3 d; vec3 e;"
                                                             "};"
                                                             "out float x; void main() { x = a.x + b + c.x + d.x + e.x; }");
    auto prog = Program::create(shader);
    CHECK(prog->verifyUniformBuffer("Buffer", ub));
}

TEST_CASE("Std140, UB3")
{
    struct TestUB3
    {
        std140vec3 a;
        std140ivec3 b;
        std140vec3 c;
        std140dvec3 d;
        std140uvec3 e;
    };
    CHECK(sizeof(std140vec3) == 3 * sizeof(float));

    auto ub = UniformBuffer::create();
    ub->addVerification({
        {&TestUB3::a, "a"}, //
        {&TestUB3::b, "b"}, //
        {&TestUB3::c, "c"}, //
        {&TestUB3::d, "d"}, //
        {&TestUB3::e, "e"},
    });
    auto shader = Shader::createFromSource(GL_VERTEX_SHADER, "layout(std140) uniform Buffer { "
                                                             "vec3 a; ivec3 b; vec3 c; dvec3 d; uvec3 e;"
                                                             "};"
                                                             "out float x; void main() { x = a.x + b.x + float(c.x) + float(d.x) + e.x; }");
    auto prog = Program::create(shader);
    CHECK(prog->verifyUniformBuffer("Buffer", ub));
}
