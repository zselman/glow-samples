// This file is auto-generated and should not be modified directly.
#include <doctest.hh>

#include <glm/glm.hpp>

#include <glow/objects/Texture2D.hh>

using namespace glow;

TEST_CASE("Texture2D, Binding")
{
    auto tex0 = Texture2D::create();
    CHECK(Texture2D::getCurrentTexture() == nullptr);

    {
        auto btex0 = tex0->bind();
        CHECK(Texture2D::getCurrentTexture() == &btex0);

        auto tex1 = Texture2D::create();
        auto tex2 = Texture2D::create();
        CHECK(Texture2D::getCurrentTexture() == &btex0);

        {
            auto btex1 = tex1->bind();
            CHECK(Texture2D::getCurrentTexture() == &btex1);

            auto btex2 = tex2->bind();
            CHECK(Texture2D::getCurrentTexture() == &btex2);
        }

        CHECK(Texture2D::getCurrentTexture() == &btex0);
    }

    CHECK(Texture2D::getCurrentTexture() == nullptr);
}
