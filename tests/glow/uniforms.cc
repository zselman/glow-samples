#include <doctest.hh>

#include <array>

#include <glm/ext.hpp>
#include <typed-geometry/tg.hh>

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/ShaderStorageBuffer.hh>

TEST_CASE("glow uniform interface")
{
    auto csh = glow::Shader::createFromSource(GL_COMPUTE_SHADER, R"(
                                              layout(local_size_x = 1) in;

                                              layout(std430) buffer bData {
                                                float data[];
                                              };

                                              uniform int uInt;
                                              uniform bool uBool;
                                              uniform vec4 uVec4;
                                              uniform float uNewFloat;
                                              uniform int uIntArray[7];
                                              uniform vec3 uPos3;
                                              uniform vec3 uVec3s[4];
                                              uniform mat4 uMat4;
                                              uniform mat3 uMat3s[2];
                                              uniform bool uBools[5];
                                              uniform vec3 uColor3s[3];
                                              uniform uvec3 uUVec3;
                                              uniform float uFloatCArray[4];
                                              uniform vec3 uDir3;
                                              uniform vec4 uColor4;
                                              uniform mat4 uTransforms[3];
                                              uniform float uFloats[4];

                                              void main()
                                              {
                                                int i = 1;
                                                data[i++] = uInt;
                                                data[i++] = uBool ? 42 : 9;
                                                data[i++] = uVec4[0];
                                                data[i++] = uVec4[1];
                                                data[i++] = uVec4[2];
                                                data[i++] = uVec4[3];
                                                data[i++] = uNewFloat;
                                                data[i++] = uIntArray[0];
                                                data[i++] = uIntArray[6];
                                                data[i++] = uPos3[0];
                                                data[i++] = uPos3[1];
                                                data[i++] = uPos3[2];
                                                data[i++] = uVec3s[0].z;
                                                data[i++] = uVec3s[2].x;
                                                data[i++] = uVec3s[3].y;
                                                data[i++] = uMat4[0][0];
                                                data[i++] = uMat4[1][2];
                                                data[i++] = uMat4[3][1];
                                                data[i++] = uMat4[3][3];
                                                data[i++] = uMat3s[0][1][2];
                                                data[i++] = uMat3s[1][2][0];
                                                data[i++] = uBools[0] ? 13 : 7;
                                                data[i++] = uBools[3] ? 13 : 7;
                                                data[i++] = uBools[4] ? 13 : 7;
                                                data[i++] = uColor3s[0].r;
                                                data[i++] = uColor3s[1].g;
                                                data[i++] = uColor3s[2].b;
                                                data[i++] = uUVec3.x;
                                                data[i++] = uUVec3.y;
                                                data[i++] = uUVec3.z;
                                                data[i++] = uFloatCArray[0];
                                                data[i++] = uFloatCArray[1];
                                                data[i++] = uFloatCArray[2];
                                                data[i++] = uFloatCArray[3];
                                                data[i++] = uDir3[0];
                                                data[i++] = uColor4[0];
                                                data[i++] = uTransforms[0];
                                                data[i++] = uFloats[2];
                                              }
                                                                 )");
    auto prog = glow::Program::create(csh);

    // create empty SSBO
    auto data = std::vector<float>();
    data.resize(100, 0.0f);
    auto ssbo = glow::ShaderStorageBuffer::create(data);
    prog->setShaderStorageBuffer("bData", ssbo);

    tg::rng rng;
    auto randomMat3 = [&]() -> tg::mat3 {
        tg::mat3 m;
        m[0] = uniform_vec(rng, tg::aabb3(-2, 2));
        m[1] = uniform_vec(rng, tg::aabb3(-2, 2));
        m[2] = uniform_vec(rng, tg::aabb3(-2, 2));
        return m;
    };

    auto mat4 = glm::lookAt(glm::vec3{5, 6, 7}, glm::vec3{2, 3, 4}, glm::vec3{0, 1, 0});
    auto mat3s = std::vector{randomMat3(), randomMat3()};

    tg::array<tg::mat4, 3> transforms;
    transforms[0] = tg::mat4::identity;
    float floats[4] = {4, 5, 6, 7};

    // launch compute shader
    {
        auto shader = prog->use();
        shader.setUniform("uInt", 17);
        shader.setUniform("uBool", true);
        shader.setUniform("uVec4", glm::vec4(1, 2, 3, 4));

        shader.uniform<float>("uNewFloat") = 12.5f;

        shader.uniform<int[]>("uIntArray") = {2, 3, 4, 5, 6, 7, 8};

        shader.uniform<tg::pos3>("uPos3") = {10, 11, 12};
        shader.uniform<tg::vec3[]>("uVec3s") = {{1, 2, 3}, {5, 6, 7}, {-1, -2, 3}, {-8, -9, -10}};

        shader.setUniform("uMat4", mat4);

        auto u = shader.uniform<tg::mat3[]>("uMat3s");
        u = mat3s;

        shader.uniform<bool[]>("uBools") = std::array<bool, 5>{{true, false, true, true, false}};

        shader.uniform<tg::color3[]>("uColor3s") = {tg::color3::white, tg::color3::red, tg::color3::magenta};

        shader.uniform<tg::usize3>("uUVec3") = {13, 14, 15};

        float a[4] = {4, 5, 6, 7};
        shader.uniform<float[]>("uFloatCArray") = a;

        shader["uDir3"] = tg::dir3::pos_x;

        shader["uColor4"] = tg::color4::red;

        shader["uTransforms"] = transforms;
        shader["uFloats"] = floats;

        shader.compute(1);
    }

    // verify data
    {
        data = ssbo->bind().getData<float>();

        auto i = 0;
        CHECK(data[i++] == 0);
        CHECK(data[i++] == 17);
        CHECK(data[i++] == 42);
        CHECK(data[i++] == 1);
        CHECK(data[i++] == 2);
        CHECK(data[i++] == 3);
        CHECK(data[i++] == 4);
        CHECK(data[i++] == 12.5f);
        // uIntArray
        CHECK(data[i++] == 2);
        CHECK(data[i++] == 8);
        // uPos3
        CHECK(data[i++] == 10);
        CHECK(data[i++] == 11);
        CHECK(data[i++] == 12);
        // uVec3s
        CHECK(data[i++] == 3);
        CHECK(data[i++] == -1);
        CHECK(data[i++] == -9);
        // uMat4
        CHECK(data[i++] == mat4[0][0]);
        CHECK(data[i++] == mat4[1][2]);
        CHECK(data[i++] == mat4[3][1]);
        CHECK(data[i++] == mat4[3][3]);
        // uMat3s
        CHECK(data[i++] == mat3s[0][1][2]);
        CHECK(data[i++] == mat3s[1][2][0]);
        // uBools
        CHECK(data[i++] == 13);
        CHECK(data[i++] == 13);
        CHECK(data[i++] == 7);
        // uColor3s
        CHECK(data[i++] == 1);
        CHECK(data[i++] == 0);
        CHECK(data[i++] == 1);
        // uUVec3
        CHECK(data[i++] == 13);
        CHECK(data[i++] == 14);
        CHECK(data[i++] == 15);
        // uFloatCArray
        CHECK(data[i++] == 4);
        CHECK(data[i++] == 5);
        CHECK(data[i++] == 6);
        CHECK(data[i++] == 7);
        // uDir3
        CHECK(data[i++] == 1);
        // uColor4
        CHECK(data[i++] == 1);
        // uTransforms[0][0]
        CHECK(data[i++] == 1);
        // uFloats[2]
        CHECK(data[i++] == 6);
    }
}
