#version 430 core

uniform sampler2DRect uTexIn;

out vec4 fOut;

void main() {
    ivec2 size = textureSize(uTexIn);
    uint idx = uint(gl_FragCoord.x) + uint(gl_FragCoord.y) * uint(size.x);
    //idx = wang_hash(idx);
    uint x = idx % size.x;
    uint y = (idx / size.x) % size.y;
    fOut = texture(uTexIn, vec2(x, y));
    //fOut = texture(uTexIn, gl_FragCoord.xy);
}
